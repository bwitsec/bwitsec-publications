

function detectVersion()
{
	version = parseInt(navigator.appVersion);
	return version;
}

function detectOS()
{
	if(navigator.userAgent.indexOf('Win') == -1)
	{
		OS = 'Macintosh';
	}
	else
	{
		OS = 'Windows';
	}
	return OS;
}

function detectBrowser()
{
	if(navigator.appName.indexOf('Netscape') == -1)
	{
		browser = 'IE';
	}
	else
	{
		browser = 'Netscape';
	}
	return browser;
}

function openWindow(pageName, windowName, width, height, moveToX, moveToY, scrollStatus, menubar)
{
	var adjWidth;
	var adjHeight;
	
	if((detectOS() == 'Macintosh') && (detectBrowser() == 'Netscape'))
	{
		adjWidth = 20;
		adjHeight = 35;
	}
	if((detectOS() == 'Macintosh') && (detectBrowser() == 'IE'))
	{
		adjWidth = 20;
		adjHeight = 35;
		winOptions = 'fullscreen=yes';
	}
	if((detectOS() == 'Windows') && (detectBrowser() == 'Netscape'))
	{
		adjWidth = 20;
		adjHeight = 35;
	}
	if((detectOS() == 'Windows') && (detectBrowser() == 'IE'))
	{
		adjWidth = 15;
		adjHeight = 35;
	}

	if(detectVersion() < 4)
	{
		self.location.href = 'oldbrowser.html';
	}
	else
	{
		if(width == 0)
		{
			width = screen.availWidth;
		}
		
		if(height == 0)
		{
			height = screen.availHeight;
		}
	
		var winWidth = width - adjWidth;
		var winHeight = height - adjHeight;
		var winSize = 'width=' + winWidth + ',height=' + winHeight +',left=' + moveToX + ',top=' + moveToY;
		var winStats = winSize+',toolbar=no,location=no,directories=no,menubar='+ menubar +',';
		winStats += 'scrollbars='+ scrollStatus +',status=no,resize=no';
		var thewindow = window.open(pageName, windowName, winStats);
		//thewindow.moveTo(moveToX,moveToY);
	}
}// end fkt

function imgWindow(width,height,imgLocation)
{
	var windowName = "Image";
	var moveToX = 50;
	var moveToY = 50;
	var scrollStatus = "no";
	var menueBar = "no";
	openWindow(imgLocation, windowName, width, height, moveToX, moveToY, scrollStatus, menueBar);
}

function openWindowBibList(pageName, windowName)
{
	var width = 800; 
	var height = 600;
	var moveToX = 50;
	var moveToY = 50;
	var scrollStatus = "yes";
	var menueBar = "yes";
	openWindow(pageName, windowName, width, height, moveToX, moveToY, scrollStatus, menueBar);
}

function openWindowWaring(pageName, windowName)
{
	var width = 500; 
	var height = 200;
	var moveToX = 200;
	var moveToY = 200;
	var scrollStatus = "no";
	var menueBar = "no";
	openWindow(pageName, windowName, width, height, moveToX, moveToY, scrollStatus, menueBar);
}

function openImagePopUp(sImage, nWidth, nHeight)
{
	var win = window.open(sImage, "Image", "resizable=yes, scrollbars=yes, width="+ nWidth +", height=" + nHeight );
	win.focus();
}

function openImagePopUpBinary(sSrc, nWidth, nHeight)
{
	var win = window.open("about:blank", "Image", "resizable=yes, width="+ nWidth +", height=" + nHeight );
	win.document.write('<img name="myImage" src="'+sSrc+'" width="'+ nWidth +'" height="'+ nHeight +'">');
	win.focus();
}

function openImagePopUpBinaryZoomed(sSrc, nWidth, nHeight, nWidthFull, nHeightFull)
{
	var win = window.open("about:blank", "Image", "resizable=yes, scrollbars=yes, width="+ nWidthFull +", height=" + nHeight );
	win.document.write('<img name="myImage" src="'+sSrc+'" width="'+ nWidthFull +'" height="'+ nHeightFull +'">');
	win.focus();
}
