%!TEX root = ../report.tex
\section{Solution}\label{sec:solution}

As shown in \autoref{sec:state} existing solutions pose significant overhead in application changes, user management, and maintenance of additional services.
While this might be appropriate in certain application domains, the overhead still leads to increases in investment and operation costs which are hard to justify.

Especially the public authorities have strict budgets and low to none economic pressure for additional security.
Universities in general also are attractive targets for attackers due to their open and diverse infrastructure and the potential theft of knowledge and technology.
Thus, some general conditions exist, that highly influence the probability of the implementation of additional security measures \autoref{img:conditions}.

\begin{figure}[th]
	\centering
	\includegraphics[width=.9\columnwidth]{img/conditions}
	\caption{ \label{img:conditions} The conditions for additional security measures in the public sector.}
\end{figure}

\begin{enumerate}
	\item Few changes to existing applications
	\item Little to none additional user management
	\item Easy to maintain
	\item Work on all major operating systems
	\item Free and open source or little to no cost
\end{enumerate}

Our solution is based on the observation, that the vast majority of web applications use the user's email address -- or the local part of the email address -- as the user's login name.
This holds for most public web services, like Amazon or eBay, but also for many public or private organizations and companies that operate their own email infrastructure.
Usually, the user's email user name\slash{}address is also his unique identifier (UID) used for organization-wide authentication.
If the email address is not used for authentication in the organization, another UID scheme usually is in place.
X.509 certificates must also be unique for a user and demand the usage of a `distinguished name' as subject.
This subject can also include the UID, which can then be used for authentication instead of the user's email address.

Many organizations also either have their own certificate authority, or use the services of a public or commercial certificate authority.
Especially the public authorities are likely to operate their own public certificate authority for use in government agencies or educational institutions.

While client certificates slowly take off, particularly in areas where trust or confidentiality is crucial, X.509 certificates are still usually found on servers and used for host authentication and SSL\slash{}TLS encryption.
Because of this, support for X.509 client certificates has long been neglected.
However, interest in stronger authentication schemes and X.509 client certificates as well as cybersecurity in general has risen in recent years.
This is due to a combination of events, including Snowden's leak of thousands of confidential NSA documents and well-known attacks on cloud providers (e.g. Apple's iCloud ``celebrity breaches'').
As a result application developers and service providers have started to adopt a variety of multi-factor authentication schemes, from relying on trusted devices (Apple) to hardware tokens (Amazon, Github).

Still, aside from specialized applications that use smartcard authentication, X.509 client certificates are usually only used on web servers to protect locations or directories.
For example, anyone providing a valid certificate signed by a specific authority is allowed to access the login screen of an application (\autoref{img:cert-wall}).
While this adds another authentication factor this cannot be considered 2FA, since there is no process that actually validates that the owner of the certificate is the same entity that tries to login to the application.
The reason for this is because the certificate request is issued by the web server while the login is requested by the application.

%\begin{figure}[th]
%	\centering
%	\includegraphics[width=.8\columnwidth]{img/cert-wall}
%	\caption{ \label{img:cert-wall} A typical certificate ``wall''. The server requests a valid certificate from the client to allow access to a location.}
%\end{figure}

\subsection{2FA with client certificates}

In order to enable true multi-factor authentication the application needs to check the identity of the client certificate and somehow match that to the identity of the user trying to login.
This requires additional effort in identity management to provide this missing link.

We observed, that for most web-based applications, users login with their email address or the local-part of their email address.
Since X.509 user certificates are issued for a specific email address, it is surprisingly easy to extract the user name directly from the certificate on the server and use this as a validated remote user for the application (\autoref{img:usercert}).
If the web server supports client certificate parsing, this can even be done directly on the web server.
If not, the web server needs to expose the client certificate to the application which in turn parses the certificate and extracts the user name.

\begin{figure}[th]
	\centering
	\includegraphics[width=.6\columnwidth]{img/usercert}
	\caption{ \label{img:usercert} A X.509 user certificate includes the user's email address in an extension field. This information can be extracted and then used as the user's login name for applications.}
\end{figure}

In our scheme, the webserver requests the user's certificate, checks the validity, and then exposes the certificate to the application.
The application than extracts the user's username directly from the (validated) certificate and sets the username server-side.
Note, that the client or user is not able to actually change or set the username, since this is completely done server-side.
The user only provides his password to complete the authentication.
The process is shown in \autoref{img:cert2fa}.

\begin{figure}[th]
	\centering
	\includegraphics[width=.8\columnwidth]{img/cert2fa}
	\caption{ \label{img:cert2fa} 2FA with X.509 user certificates. The webserver requests a user certificate, checks the validity and exposes it to the application, which extracts the username. The user only has to provide his password.}
\end{figure}

Apache2 supports the necessary options to extract the since version 2.4.12.
Prior to that, the client certificate must be exposed to the application.
\autoref{lst:apache2} shows the configuration for Apache2.
The important part is the export of the certificate and the environment variables.

\begin{lstlisting}[language=bash, basicstyle=\footnotesize, frame=single, breaklines=True, label=lst:apache2, caption=Apache config]
<VirtualHost *:443>
...
SSLVerifyClient require
SSLVerifyDepth 3
SSLOptions +ExportCertData +StdEnvVars
...
</VirtualHost>
\end{lstlisting}

On the application side, parsing the client certificate and extracting the email address can be achieved in few lines of code (\autoref{lst:login1}).

\begin{lstlisting}[language=php, basicstyle=\footnotesize, frame=single, breaklines=True, label=lst:login1, caption=Email address extraction with apache $<$ 2.4.12]
$clientcert = openssl_x.509_parse($_SERVER['SSL_CLIENT_CERT']);
$subjectaltname = $clientcert['extensions']['subjectAltName'];
$email = substr($subjectaltname,strpos($subjectaltname,":")+1);
\end{lstlisting}

With Apache2 version 2.4.12 or later, extraction of the user's email address can be done with a single line of code (\autoref{lst:login2}), since support to access the required field is already built-in.
\begin{lstlisting}[language=php, basicstyle=\footnotesize, frame=single, breaklines=True, label=lst:login2, caption=Email address extraction with apache $\ge$ 2.4.12]
$email = $_SERVER['SSL_CLIENT_SAN_Email_0'];
\end{lstlisting}

In any case, \autoref{lst:login1} shows the appropriate login form for the application.
The form uses the formerly extracted email address as readonly value for the user name.
The user is thus not able to actively change the login name.
Since the web server verifies the authenticity of the client certificate, the user's email address is also verified.
It is important to note, that the user is not allowed to change the username in any way.
That is, setting the username must be done server-side, not in a POST evaluation.
Applications usually set the username in the login page POST evaluation.
This must also be removed from code, to prevent client side attacks.

\begin{lstlisting}[language=html, basicstyle=\footnotesize, frame=single, breaklines=True, label=lst:loginform, caption=Login form with readonly user name]
<form>
<label><b>Username: </b></label>
<input type='text' name='user' value='<?php echo $email; ?>' readonly /> <br/>

<label><b>Password: </b></label>
<input type="password" placeholder="Enter Password" name="psw" required />

<button type="submit">Login</button>
<button type="button">Cancel</button>
</form>
\end{lstlisting}


\subsection{Securing web applications}

The proposed scheme can be used to efficiently secure any kind of web application with multiple authentication factors.
A remaining issue is the storage location of the client certificate itself.
Saving the certificate in the browser's local store exposes the private key to a number of attacks that might lead to it's loss.

Fortunately, our solution can be easily combined with hardware tokens like smartcards or USB cryptokens to ensure that the private key stays private, even if the user's machine is compromised.
Not all browsers support hardware-based certificate stores, though.
Depending on the required trust level, the application can be secured accordingly.

%\autoref{img:evolution} shows the different stages, or combinations, to add certificate-based authentication.

%\begin{figure}[t]
%\centering
%\includegraphics[width=.8\columnwidth]{img/Evolution-crop}
%\caption{\label{img:evolution}The different stages of securing a web application with certificate-based second factors. Adding a software certificate provides an additional factor without linking it to the users identity. Application support is required to link users with their certificate. Instead of storing the certificate in software, it can be stored on a hardware token, to guarantee it stays private.}
%\end{figure}

\subsubsection{Example use cases}

The ease with which our authentication scheme can be implemented allows many applications.

\begin{description}
	\item[Web Server or Proxy] Using http auth and authentication modules, the whole web server can be secured.
	%Given web server support, the user name can be extracted directly in the site configuration simply by accessing the certificate field \texttt{SSL\_CLIENT\_SAN\_Email\_0}.
	To quickly add 2FA support to multiple applications or servers, an authentication proxy could even be used.
	
	\item[Locations\slash{}Specific Applications] Secure only specific locations or applications served by the web server, using true two-factor authentication.
	
	\item[Specific Actions] Using the option \texttt{QUERY\_STRING} to match post parameters or keywords in the URL, additional authentication factors can be requested for these specific actions.
\end{description}
