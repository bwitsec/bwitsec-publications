%!TEX root = ../report.tex
\section{State of the Art}\label{sec:state}

Support for Multi-factor authentication in daily applications is steadily increasing.
Many prominent service providers or manufacturers have started to adopt MFA in one form or another.
Hardware and software developers also react to newer security threats and recent years have seen a growing number of products, services, and applications regarding multi-factor authentication; be it support in established software, new hardware tokens, or additional authentication services.

There are several different methods that are commonly used, which are often similar in use but differ in technology.
The following sections provide an overview of contemporary authentication technology.

\subsection{Trusted Channels}

This is probably one of the earliest and still most common ways of authentication with multiple factors.
The service provider keeps a record of trusted clients the user has used in the past, as well as some contact information that is verified to be in the user's possession, usually the email address or a phone number.
Some service providers that also offer hardware also allow the user to register a trusted device, usually a phone or tablet, that is in his possession and serves as proof of ownership.

When the user accesses the service from a new device or client - like another browser - the service prompts the user to enter a secret which is sent to his trusted contact address or trusted device.

Many web service providers have adopted this scheme, including Steam, Amazon, Facebook, Apple, and Google.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=.7\columnwidth]{img/trusted}
	\caption{ \label{img:trusted} Multi-factor authentication with trusted channels. When a new client is used the service sends a security code to a trusted device or email address.}
\end{figure}

\subsection{One-Time-Password (OTP)}

A One-Time-Password is a secret that can only be used a once.
An important aspect is how to compute and distribute the password and and how to ensure that both the service and the client are aware of the same password.

With OTP, the user and the service provider share a common static secret which is concatenated with a variable.
This secret should be kept on a dedicated device to fulfill the ``what you have'' premise.
The result is then hashed multiple times to generate the pseudo-random password, known only to the parties knowing the secret and the variable (see \autoref{img:otp}).

\begin{figure}[th]
	\centering
	\includegraphics[width=.7\columnwidth]{img/otp}
	\caption{ \label{img:otp} OTP authentication. The service and the user share a common secret, represented by a QR code or a hex\slash{}base32 encoded number. The user can use a smartphone app, like FreeOTP, or hardware devices, like Yubikey or Nitrokey, to generate the authentication code. \newline \footnotesize{Image sources \url{https://github.com}} and \url{https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp}.}
\end{figure}

Two methods have been established, the counter-based OTP (HOTP) and the time-based OTP (TOTP).
With HOTP, service and client keep track of a counter that is used as variable input.
The counter is incremented each time the OPT function is used and has to remain in sync in order for the parties to compute the correct OTP value.
This introduces significant sync problems, which are somewhat mitigated by allowing a window of counters.

TOTP on the other hand uses the current time as variable input.
The prevents that counters can run out-of-sync but requires that the clocks of server and client are synchronized.

OTPs have gained momentum in user acceptance and distribution due to the availability of smartphone apps like the `google authenticator' or `FreeOTP' which allow easy management and usage of OTPs for multiple services.
Many online services have incorporated OTP, including Github and Amazon.
There are many devices, applications, and services available, the make use of OTPs, both proprietary as well as free and open source.

For linux and SSH authentication there are libpam-google-authenticator\footnote{\url{https://github.com/google/google-authenticator-libpam}} and libpam-oath\footnote{\url{http://www.nongnu.org/oath-toolkit/}}.
We highly discourage using libpam-google-authenticator, since the built-in key-generator leaks all private information, like the secret key, to Google to generate the QR code for the smartphone app.
A similar module exists for Apache (`mod-authn-otp')\footnote{\url{https://github.com/archiecobbs/mod-authn-otp}}
There are also open source authentication servers like `LinOTP' and its fork `PrivacyIdea' that aim to provide a central second factor authentication service usable by many applications (like e.g. LDAP).
These services also offer other second factor methods built-in or in form of plug-ins.

While OTP is easy to use for the consumer, it requires significant changes to applications and extended user management.
LinOTP\slash{}PrivacyIdea try to mitigate the user management problem by providing a self service portal that allows users to manage their own second factors.
Ironically, the self service portal does not allow two-factor authentication, which takes the whole service {\em ad absurdum}.


\subsection{Transaction Authentication Number (TAN)}

Transaction authentication numbers are prominently used in bank related transactions. They are a form of a one-time-password in the sense, that they also are valid only for a single transaction.
Initially, TANs where used not to authenticate a user or session, but to authenticate critical transactions like money transfers.
They were been distributed in paper form as TAN-lists and the user was responsible for managing the list and keep the numbers safe.
With the ubiquity of mobile phones the standard now is to send the user a SMS containing the valid TAN.
The so-called SMS-TAN method is also used for user authentication and sometimes combined with the trusted devices / contacts methods above.

\begin{figure}[th]
	\centering
	\includegraphics[width=.7\columnwidth]{img/tan}
	\caption{ \label{img:tan} Examples for TAN generation. Left: a classical printed TAN list. Middle: SMS-TAN. Right: chip-TAN \newline \footnotesize{Image source : \url{https://commons.wikimedia.org/wiki/File:SmartTAN_optic-Gadget.jpg}}}
\end{figure}

\subsection{Universal 2nd Factor (U2F)}

A new-comer is the universal second factor, or U2F.
It is based on standard private\slash{}public key cryptography, much like TLS.
The user has a secure token that generates the private key and then publishes the associated public key to the online service.
When trying to login, the online service uses the public key to generate a challenge and sends that to the user's U2F device.
Only if the user's device can solve pass the challenge, the user is authenticated.

\begin{figure}[th]
	\centering
	\includegraphics[width=.7\columnwidth]{img/u2f}
	\caption{ \label{img:u2f} Registration of a U2F device. The user needs to use a supported browser that is capable of communicating with the U2F device. The device itself can be a Yubikey or any other U2F capable token.\newline \footnotesize{Yubikey image source: \url{https://www.yubico.com/}}}
\end{figure}

U2F requires a secure token as well as client software that can communicate with the token.
The web service must also be made aware of U2F.
At the time of this writing, Chrome is the only browser that supports U2F out-of-the-box.
The prime developer Yubico also provides a REST enabled authentication service that online service providers can use to reduce their implementation and maintenance costs.
Yubicos authentication servers are open source and can also be self hosted.

\subsection{X.509 User Certificates}

X.509 Client certificates for authentication do not see widespread distribution, at least in consumer products.
They are mostly found in areas with restricted access, usually in the form of smartcards.

Web server support for client certificates has been around for years, however, the common use case is what we call a `certwall' \autoref{img:cert-wall}.
That is, access to a resource on the server is only granted with a valid certificate signed by a specific authority.
If access must only be allowed to certain people, the webserver can also be configured to only allow specific certificates, identified by a unique identifier like the serial number or the dinstinguished name (DN).

\begin{figure}[th]
	\centering
	\includegraphics[width=.8\columnwidth]{img/cert-wall}
	\caption{ \label{img:cert-wall} A typical ``certwall''. The server requests a valid certificate from the client to allow access to a location. The user then authenticates with his username and password. No crosschecking of the certificate and user's identity is performed.}
\end{figure}

However, this form of certificate authentication is not a valid form of multi-factor authentication, since the identity of the user is not crosschecked with the identity provided by the client certificate.
Ironically, it still requires extensive additional user management.

\subsection{Verdict}

Although there are a multitude of available methods and products to choose from, we found that most of them are not suitable for widespread deployment, especially in heterogeneous and complex environments like those found on university campuses.

The products either won't support all the required platforms or have too high requirements on production and maintenance.
Some introduce additional dependencies on additional, maybe external, services.

For widespread implementation and both admin as well as user acceptance, the solution must be simple and introduce very little additional costs.